import types from "../types";

const cartReducer = (state = [], action) => {
	switch (action.type) {
		case types.setCart: {
			return action.payload.cart
		}
		case types.confirmCartPurchase: {
			return []
		}
		default: {
			return state
		}
	}
}

export default cartReducer