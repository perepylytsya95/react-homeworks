import favoritesReducer from "./reducer";
import types from "../types";

describe('favorite reducer', () => {
	const currentState = [{ SKU: 1, price: 10, name: 'Shark' }, { SKU: 2, price: 11, name: 'Cutter' }]
	const newState = [{ SKU: 2, price: 11, name: 'Cutter' }, { SKU: 3, price: 12, name: 'Boy' }]

	it('should return default value', () => {
		expect(favoritesReducer(currentState, {
			type: 'unknown type',
			payload: 'any data'
		})).toEqual(currentState)
	})

	it('should update favorites', () => {
		expect(favoritesReducer(currentState, {
			type: types.setFavorites,
			payload: {
				favorites: newState
			}
		})).toEqual(newState)
	})
})