import types from "../types";

export const setModal = (modalInfo) => {
	return {
		type: types.setModal,
		payload: modalInfo
	}
}