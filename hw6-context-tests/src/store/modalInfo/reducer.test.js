import modalInfoReducer from './reducer';
import types from '../types';

describe('modal reducer', () => {
	const currentState = { noModal: false, productData: 'some data', modalType: 'some type' }
	const newState = { noModal: true, productData: 'another data', modalType: 'another type' }

	it('should return default value', () => {
		expect(modalInfoReducer(currentState, {
			type: 'unknown type',
			payload: 'any data'
		})).toEqual(currentState)
	})

	it('should update modal info', () => {
		expect(modalInfoReducer(currentState, {
			type: types.setModal,
			payload: newState
		}))
	})
})