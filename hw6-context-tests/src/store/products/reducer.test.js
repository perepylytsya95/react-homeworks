import productsReducer from "./reducer";
import types from "../types";

describe('products reducer', () => {
	const initialState = []
	const currentState = [{ SKU: 1, price: 10, name: 'Shark' }, { SKU: 2, price: 11, name: 'Cutter' }]
	const requestedData = [{ SKU: 1, price: 10, name: 'Shark' }, { SKU: 2, price: 11, name: 'Cutter' }]

	it('should return default value', () => {
		expect(productsReducer(currentState, {
			type: 'unknown type',
			payload: 'any data'
		})).toEqual(currentState)
	})

	it('should set loading of error message', () => {
		expect(productsReducer(initialState, {
			type: types.getProductsRequested,
			payload: { message: 'loading' }
		})).toEqual('loading')
		expect(productsReducer(initialState, {
			type: types.getProductsError,
			payload: { message: 'error' }
		})).toEqual('error')
	})

	it('should set products upon successful request', () => {
		expect(productsReducer(initialState, {
			type: types.getProductsSuccess,
			payload: {
				products: requestedData
			}
		})).toEqual(requestedData)
	})
})