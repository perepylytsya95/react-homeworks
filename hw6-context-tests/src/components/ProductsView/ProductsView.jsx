import React, { useContext } from 'react';
import './productsView.scss';
import { ReactComponent as CardsView } from '../../svg/cards.svg';
import { ReactComponent as TableView } from '../../svg/table.svg';
import ProductsViewContext from '../../context/ProductsViewContext';
import classnames from 'classnames';

const ProductView = () => {
	const {contextView, setContextView} = useContext(ProductsViewContext)

	const toggleContextView = (selectedView) => selectedView === contextView ? null : setContextView(selectedView)

	return (
		<div className='view'>
			<div className='view__wrapper'>
				<div onClick={() => toggleContextView('table')} className='view__item'>
					<TableView className={classnames('view__logo', {'view-selected': contextView === 'table'})} />
				</div>

				<div onClick={() => toggleContextView('cards')} className='view__item'>
					<CardsView className={classnames('view__logo', {'view-selected': contextView === 'cards'})} />
				</div>
			</div>
		</div>
	);
};

export default ProductView;