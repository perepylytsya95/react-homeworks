import Header from "./Header";
import { render } from '@testing-library/react';

describe('Header component', () => {
	it('should render', () => {
		const { container } = render(<Header></Header>)
		expect(container.firstChild).toMatchSnapshot()
	})
})