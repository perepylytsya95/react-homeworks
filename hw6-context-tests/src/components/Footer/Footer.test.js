import Footer from "./Footer";
import { render } from '@testing-library/react';

describe('Footer component', () => {
	it('should render', () => {
		const { container } = render(<Footer></Footer>)
		expect(container.firstChild).toMatchSnapshot()
	})
})