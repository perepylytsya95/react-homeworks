import Modal from './Modal';
import userEvent from '@testing-library/user-event';
import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux';
import { legacy_createStore as createStore } from 'redux';
import { reducer } from '../../store/store';

describe('Modal component', () => {
	describe('addToCart modal', () => {
		const state = {
			modalInfo: {
				noModal: false,
				productData: {},
				modalType: 'addToCart',
			}
		}
		it('should render addToCart modal component', () => {
			const store = createStore(reducer, state)
			render(
				<Provider store={store}>
					<Modal />
				</Provider>
			)
			screen.getByText(/Add to cart?/)
		})
		it('should pass onClick if addToCart button is clicked', () => {
			const store = createStore(reducer, state)
			const handleClick = jest.fn()
			const {container} = render(
				<Provider store={store}>
					<Modal addToCart={handleClick} />
				</Provider>
			)
			const button = container.querySelector('#confirm-adding')
			userEvent.click(button)
			expect(handleClick).toHaveBeenCalled()
		})
	})

	describe('removeFromCart modal', () => {
		const state = {
			modalInfo: {
				noModal: false,
				productData: {},
				modalType: 'removeFromCart',
			}
		}
		it('should render removeFromCart modal component', () => {
			const store = createStore(reducer, state)
			render(
				<Provider store={store}>
					<Modal />
				</Provider>
			)
			screen.getByText(/Remove from cart?/)
		})
		it('should pass onClick if removeFromCart button is clicked', () => {
			const store = createStore(reducer, state)
			const handleClick = jest.fn()
			const {container} = render(
				<Provider store={store}>
					<Modal removeFromCart={handleClick} />
				</Provider>
			)
			const button = container.querySelector('#confirm-removing')
			userEvent.click(button)
			expect(handleClick).toHaveBeenCalled()
		})
	})
})