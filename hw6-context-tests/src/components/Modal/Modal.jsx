import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setModal } from '../../store/modalInfo/action';
import './modal.scss';

const Modal = ({ addToCart, removeFromCart }) => {
	const modal = useSelector(state => state.modalInfo)
	const dispatch = useDispatch()
	const [addProductCount, setAddProductCount] = useState(1)
	const [removeProductCount, setRemoveProductCount] = useState(modal.productData.quantity)

	const closeModal = (event) => {
		event.stopPropagation()
		const conditionToClose = event.target.classList.contains('popup__close')
			|| event.target.classList.contains('popup__body')
			|| event.target.classList.contains('popup__action')

		if (conditionToClose) {
			dispatch(setModal({ ...modal, noModal: true }))
		}
	}

	const handleAddToCartClick = (product) => {
		addToCart(product, addProductCount)
	}

	const handleRemoveFromCartClick = (product) => {
		removeFromCart(product, removeProductCount)
	}


	return (
		<div className='popup'>
			<div className='popup__body' onClick={closeModal}>

				{modal.modalType === 'addToCart' &&
					<div className='popup__content'>
						<button className='popup__close'>X</button>
						<div className='popup__title'>Add {modal.productData.name} to cart?</div>
						<div className='popup__text'>
							<img src={modal.productData.url} alt='knife' />
							<p>{modal.productData.price} USD</p>
						</div>
						<div className='product-count'>
							<button
								onClick={() => setAddProductCount(
									addProductCount === 1 ?
										addProductCount :
										addProductCount - 1)}
								className='product-btn product-add'>
								-
							</button>
							<div className='product-count__number'>{addProductCount}</div>
							<button
								onClick={() => setAddProductCount(addProductCount + 1)}
								className='product-btn product-remove'>
								+
							</button>
						</div>
						<div className='popup__action-wrapper'>
							<button
								id='confirm-adding'
								className='popup__action action1'
								onClick={() => handleAddToCartClick(modal.productData)}>
								Yes
							</button>
							<button
								className='popup__action action1'>
								No
							</button>
						</div>
					</div>}

				{modal.modalType === 'removeFromCart' &&
					<div className='popup__content'>
						<button className='popup__close'>X</button>
						<div className='popup__title'>Remove {modal.productData.name} from cart?</div>
						<div className='popup__text'>
							<img src={modal.productData.url} alt='knife' />
							<p style={{ textAlign: 'center' }}>{modal.productData.price} USD</p>
						</div>
						<div className='product-count'>
							<button
								onClick={() => setRemoveProductCount(
									removeProductCount === 1 ?
										removeProductCount :
										removeProductCount - 1)}
								className='product-btn product-add'>
								-
							</button>
							<div className='product-count__number'>{removeProductCount}</div>
							<button
								onClick={() => setRemoveProductCount(
									removeProductCount === modal.productData.quantity ?
										removeProductCount :
										removeProductCount + 1
								)}
								className='product-btn product-remove'>
								+
							</button>
						</div>
						<div className='popup__action-wrapper'>
							<button
								id='confirm-removing'
								className='popup__action action2'
								onClick={() => handleRemoveFromCartClick(modal.productData)}>
								Yes
							</button>
							<button
								className='popup__action action2'>
								No
							</button>
						</div>
					</div>}

			</div>
		</div>
	);
}

export default Modal;