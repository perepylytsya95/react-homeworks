import React, { Component } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import './app.scss'
import modalWindowDeclarations from './config/modalConfig';

class App extends Component {
	state = {
		noModal: true
	}

	displayModal = (event) => {
		const buttonId = event.target.dataset.id
		const modalToDisplay = modalWindowDeclarations.find(modal => modal.id === buttonId)
		this.setState({
			noModal: !this.state.noModal,
			...modalToDisplay
		})
	}

	closeModal = (event) => {
		event.stopPropagation()
		if(event.target.classList.contains('popup__close') || event.target.classList.contains('popup__body')){
			this.setState({
				noModal: !this.state.noModal
			})
		}
	}

	render() {
		const { noModal, header, closeButton, text, actions } = this.state

		return (
			<div className='buttons-wrapper'>
				<Button dataId='modalID1' backgroundColor='blue' text='Open first modal' onClick={this.displayModal} />
				<Button dataId='modalID2' backgroundColor='yellow' text='Open second modal' onClick={this.displayModal} />
				{noModal ? null : <Modal
					header={header}
					closeButton={closeButton}
					text={text}
					actions={actions}
					closeModal = {this.closeModal}
				/>}
			</div>
		);
	}
}

export default App;