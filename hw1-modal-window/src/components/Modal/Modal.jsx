import React, { Component } from 'react';
import './modal.scss';

class Modal extends Component {

	render() {
		const { header, closeButton, text, actions, closeModal } = this.props
		return (
			<div className='popup'>
				<div className='popup__body' onClick={closeModal}>
					<div className='popup__content'>
						{closeButton && <button className='popup__close'>X</button>}
						<div className='popup__title'>{header}</div>
						<div className='popup__text'>{text}</div>
						{actions}
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;