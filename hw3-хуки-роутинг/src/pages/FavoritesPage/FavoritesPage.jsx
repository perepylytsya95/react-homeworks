import React from 'react';
import './favoritesPage.scss'

const FavoritesPage = ({ favorites, products, toggleFavorites }) => {
	const favoritesOnly = products.filter(product => {
		return favorites.includes(product.SKU)
	})

	const handleStarClick = (SKU) => {
		toggleFavorites(SKU)
	}

	return (
		<div className='favorites-wrapper'>
			{favorites.length === 0 &&
				<div className='favorites-empty'>No items have been added to favorites.</div>}
			{favorites.length > 0 &&
				<div className='favorites-list'>
					{favoritesOnly.map(el => {
						return (
							<div key={`${el.SKU}F`} className='favorites-list__container'>
								<div className='favorites-item '>
									<div className='favorites-item__image'>
										<img src={el.url} alt='favorite knife'></img>
									</div>
									<div className='favorites-item__name'>{el.name}</div>
									<div className='favorites-item__price'>{`${el.price}$`}</div>
								</div>
								<div className='favorites-star' onClick={(event) => { handleStarClick(el.SKU) }}>
									<img src='img/star-added.png' alt='yellow star'></img>
								</div>
							</div>)
					})}
				</div>
			}
		</div>
	);
};

export default FavoritesPage;