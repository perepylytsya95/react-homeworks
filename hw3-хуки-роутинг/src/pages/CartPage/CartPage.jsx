import React from 'react';
import './cartPage.scss'

const CartPage = ({ cart, displayModal }) => {
	const openModalClick = (SKU, index) => {
		displayModal(SKU, 'removeFromCart', index)
	}

	return (
		<div className='cart-wrapper'>
			{cart.length === 0 &&
				<div className='cart-empty'>No items have been added to cart.</div>}
			{cart.length > 0 &&
				<div className='cart-list'>
					{cart.map((el, index) => {
						return (
							<div key={el.SKU + 'C' + index} className='cart-list__container'>
								<div className='cart-item '>
									<div className='cart-item__image'>
										<img src={el.url} alt='favorite knife'></img>
									</div>
									<div className='cart-item__name'>{el.name}</div>
									<div className='cart-item__price'>{`${el.price}$`}</div>
								</div>
								<div className='cart-cross' onClick={event => { openModalClick(el, index) }}>
									<button>X</button>
								</div>
							</div>)
					})}
				</div>
			}
		</div>
	);
};

export default CartPage;