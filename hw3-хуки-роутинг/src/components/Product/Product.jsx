import PropTypes from 'prop-types';
import './product.scss';

const Product = (props) => {
	const { productInfo, favorites, onDisplayModalClick, onFavoriteClick } = props

	return (
		<div className='product'>
			<h2 className='product__name' style={{ color: productInfo.color }}>{productInfo.name}</h2>
			<div className='product__img'>
				<img src={productInfo.url} alt='knife' />
			</div>
			<div className='product__price'>Price: {productInfo.price} USD</div>
			<div className='product__action'>
				{favorites.includes(productInfo.SKU) ?
					<img onClick={event => { onFavoriteClick(productInfo.SKU) }} className='product__favorite' src='img/star-added.png' alt='yellow star' /> :
					<img onClick={event => { onFavoriteClick(productInfo.SKU) }} className='product__favorite' src='img/favorite-2765.svg' alt='black star' />
				}
				<button onClick={event => { onDisplayModalClick(productInfo, 'addToCart') }} className='product__cart'>Add to cart</button>
			</div>
		</div>
	);
}

Product.defaultProps = {
	productInfo: {
		name: 'best knife',
		color: 'orange',
		price: 1,
		url: 'img/Cutter.jpg'
	}
}

Product.propTypes = {
	productInfo: PropTypes.object,
	displayModal: PropTypes.func,
	addToFavorite: PropTypes.func,
	favorites: PropTypes.array,
}

export default Product;