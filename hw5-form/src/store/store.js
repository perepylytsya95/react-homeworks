import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import cartReducer from './cart/reducer';
import favoritesReducer from './favorites/reducer';
import modalInfoReducer from './modalInfo/reducer';
import productsReducer from './products/reducer';

const preloadedState = {};

const reducer = combineReducers({
	products: productsReducer,
	favorites: favoritesReducer,
	cart: cartReducer,
	modalInfo: modalInfoReducer
})

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f => f)

const store = createStore(
	reducer,
	preloadedState,
	compose(applyMiddleware(thunk), devTools)
)

export default store