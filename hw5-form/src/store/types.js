const types = {
	setProducts: 'SET_PRODUCTS',
	setFavorites: 'SET_FAVORITES',
	setCart: 'SET_CART',
	confirmCartPurchase: 'CONFIRM_CART_PURCHASE',
	setModal: 'SET_MODAL',
}

export default types