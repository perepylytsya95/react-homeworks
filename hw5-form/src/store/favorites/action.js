import types from "../types";

export const setFavorites = (favoritesItems) => {
	return {
		type: types.setFavorites,
		payload: {
			favorites: favoritesItems
		}
	}
}