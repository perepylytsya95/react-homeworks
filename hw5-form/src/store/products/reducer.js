import types from '../types';

const productsReducer = (state = [], action) => {
	switch (action.type) {
		case types.setProducts: {
			return action.payload.products
		}
		default: {
			return state
		}
	}
}

export default productsReducer