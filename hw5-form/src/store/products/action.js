import getProducts from './../../api/getProducts';
import types from '../types';

export const setProducts = (products) => {
	return {
		type: types.setProducts,
		payload: {
			products: products
		}
	}
}

export const fetchProducts = () => (dispatch) => {
	getProducts()
		.then(allProducts => dispatch(setProducts(allProducts)))
}