import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './topBar.scss';

const TopBar = () => {
	const favorites = useSelector(state => state.favorites)
	const cart = useSelector(state => state.cart)
	const cartTotalQuantity = cart.reduce((prev, item) => item.quantity + prev, 0)

	return (
		<div className='top-bar'>
			<div className='home'>
				<Link to={''}>
					<button className='home__button'>Home</button>
				</Link>
			</div>
			<Link to={'favorites'}>
				<div className='favorites-star'>
					<img src='img/star-added.png' alt='star' />
					<span className='favorites-star__counter'>{favorites.length}</span>
				</div>
			</Link>
			<Link to={'cart'}>
				<div className='cart-logo'>
					<img src='img/shopping_cart.svg' alt='star' />
					<span className='cart-logo__counter'>{cartTotalQuantity}</span>
				</div>
			</Link>
		</div>
	);
}

export default TopBar;