import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './modal.scss';

class Modal extends Component {
	static propTypes = {
		modalSettings: PropTypes.object,
		closeModal: PropTypes.func,
		closeButton: PropTypes.bool,
		addToCart: PropTypes.func,
	}

	render() {
		const { modalSettings, closeModal, closeButton, addToCart } = this.props

		return (
			<div className='popup'>
				<div className='popup__body' onClick={closeModal}>
					<div className='popup__content'>
						{closeButton && <button className='popup__close'>X</button>}
						<div className='popup__title'>Add {modalSettings.name} to cart?</div>
						<div className='popup__text'>{(
							<>
								<img src={modalSettings.url} alt='knife' />
								<p style={{ textAlign: 'center' }}>{modalSettings.price} USD</p>
							</>
						)}</div>

						<div className='popup__action-wrapper'>
							<button data-id={modalSettings.SKU} className='popup__action action1' onClick={addToCart}>Yes</button>
							<button className='popup__action action2'>No</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

Modal.defaultProps = {
	modalSettings: {
		name: 'best knife',
		price: 1,
		url: 'img/Cutter.jpg',
	}
}

export default Modal;