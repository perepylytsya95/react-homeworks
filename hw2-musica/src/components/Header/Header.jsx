import React, { Component } from 'react';
import './header.scss'

class Header extends Component {
	render() {
		return (
			<div className='header'>
				<span className='header__text'>Buy</span>
				<img className='header__logo' src='img/header-logo.svg' alt='black knife' />
				<span className='header__text'>Knifes</span>
			</div>
		);
	}
}

export default Header;