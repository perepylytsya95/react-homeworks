const modalWindowDeclarations = [
	{
		id: 'modalID1',
		header: 'Add to cart?',
		closeButton: true,
		text: '',
		actions: (
			<div className='popup__action-wrapper'>
				<button className='popup__action1'>Action1</button>
				<button className='popup__action1'>Action2</button>
			</div>
		)
	},
	{
		id: 'modalID2',
		header: 'Second modal',
		closeButton: false,
		text: 'Малайзия – страна в Юго-Восточной Азии, занимающая часть полуострова Малакка и острова Калимантан. Она славится своими пляжами, тропическими лесами и оригинальной культурой, на которую оказали влияние малайские, китайские, индийские и европейские традиции. Столица страны Куала-Лумпур известна во всем мире благодаря колониальным зданиям, оживленному торговому району Букит-Бинтанг и небоскребам, в том числе знаменитым башням-близнецам "Петронас" высотой 451 метров. ',
		actions: (
			<div className='popup__action-wrapper'>
				<button className='popup__action2'>Action1</button>
				<button className='popup__action2'>Action2</button>
			</div>
		)
	}
]

export default modalWindowDeclarations