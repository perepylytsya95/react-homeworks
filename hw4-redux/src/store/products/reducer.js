import types from '../types';

const productsReducer = (state = [], action) => {
	switch (action.type) {
		case types.getProductsRequested: {
			return action.payload.message
		}
		case types.getProductsSuccess: {
			return action.payload.products
		}
		case types.getProductsError: {
			return action.payload.message
		}
		default: {
			return state
		}
	}
}

export default productsReducer