import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import './app.scss';

import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import TopBar from './components/TopBar/TopBar';
import Footer from './components/Footer/Footer';
import HomePage from './pages/HomePage/HomePage';
import FavoritesPage from './pages/FavoritesPage/FavoritesPage';
import CartPage from './pages/CartPage/CartPage';

import setCart from './store/cart/action';
import { setFavorites } from './store/favorites/action';
import { fetchProducts } from './store/products/action';

const App = () => {
	const favorites = useSelector(state => state.favorites)
	const cart = useSelector(state => state.cart)
	const noModal = useSelector(state => state.modalInfo.noModal)
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(fetchProducts())

		if (!localStorage.getItem('cart')) {
			localStorage.setItem('cart', JSON.stringify(cart))
		} else {
			dispatch(setCart(JSON.parse(localStorage.getItem('cart'))))
		}

		if (!localStorage.getItem('favorites')) {
			localStorage.setItem('favorites', JSON.stringify(favorites))
		} else {
			dispatch(setFavorites(JSON.parse(localStorage.getItem('favorites'))))
		}
	}, [])

	const addToCart = (cartItem, addProductCount) => {
		const inCart = cart.find(item => item.SKU === cartItem.SKU)
		let newCart = []

		if (inCart) {
			inCart.quantity += addProductCount
			newCart = [...cart]
		} else {
			cartItem.quantity = addProductCount
			newCart = [...cart, cartItem]
		}

		dispatch(setCart(newCart))
		localStorage.setItem('cart', JSON.stringify(newCart))
	}

	const removeFromCart = (cartItem, removeProductCount) => {
		let newCart = []

		if (cartItem.quantity - removeProductCount === 0) {
			newCart = cart.filter(el => el.SKU !== cartItem.SKU)
		} else {
			cartItem.quantity -= removeProductCount
			newCart = [...cart]
		}

		dispatch(setCart(newCart))
		localStorage.setItem('cart', JSON.stringify(newCart))
	}

	return (
		<div className='content-wrapper'>
			<Header />
			<TopBar />
			<div className='main'>
				<Routes>
					<Route path='/' element={<HomePage />}></Route>
					<Route path='favorites' element={<FavoritesPage />}></Route>
					<Route path='cart' element={<CartPage />}></Route>
				</Routes>
			</div>
			<Footer />
			{!noModal &&
				<Modal
					addToCart={addToCart}
					removeFromCart={removeFromCart}
				/>}
		</div>
	);
}

export default App;